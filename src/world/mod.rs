pub use self::tiles::*;

mod tiles;

pub struct World {
    pub tiles: tiles::Tiles,
}

impl World {
    pub fn new() -> Self {
        Self {
            tiles: tiles::Tiles::new(),
        }
    }
}
