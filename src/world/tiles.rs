use habanero::{collections::sparse_grid_2d::SparseGrid2d, glmh};
use ordered_float::OrderedFloat;
use rand::{prelude::IteratorRandom, prelude::ThreadRng, Rng};

pub const TILE_WIDTH: isize = 16;
pub mod tile {
    pub type Type = usize;

    pub const SOIL: Type = 0;
    pub const GRASS_GROWN: Type = 1;
    pub const GRASS_GRAZED: Type = 2;
    pub const SLIME_GROWN: Type = 3;
    pub const SLIME_GRAZED: Type = 4;
    pub const TYPE_COUNT: Type = 5;
}

pub type TileCoord = glm::TVec2<isize>;

const AUTOMATA_FREQUENCY: u32 = 60;

pub struct Tiles {
    pub grid: SparseGrid2d<tile::Type>,
    pub type_data: TileTypeData,
    pub automata_cooldown: u32,
}
pub struct DistantTileCoord {
    pub coord: TileCoord,
    pub distance: f32,
}

impl Tiles {
    pub fn new() -> Self {
        Self {
            grid: SparseGrid2d::new(),
            type_data: TileTypeData::new(),
            automata_cooldown: AUTOMATA_FREQUENCY,
        }
    }
    pub fn write_to_tilemap(&self, tilemap: &mut habanero::render_2d::Tilemap) {
        tilemap.tiles.clear();

        for tile in self.grid.iter_tiles() {
            tilemap
                .tiles
                .set(tile.coord, self.type_data.graphics[*tile.tile]);
        }
    }

    pub fn update_automata(&mut self) {
        if self.automata_cooldown == 0 {
            let old_tiles: Vec<_> = self.grid.iter_tiles().map(|t| t.cloned()).collect();
            for tile in old_tiles {
                if let Some(automata) = &self.type_data.automata[tile.tile] {
                    (automata.func)(tile.coord, &mut self.grid, &mut rand::thread_rng());
                }
                self.automata_cooldown = AUTOMATA_FREQUENCY;
            }
        } else {
            self.automata_cooldown -= 1;
        }
    }

    pub fn world_to_tile(coord: glm::Vec2) -> TileCoord {
        let to_tile = |v| (v / TILE_WIDTH as f32) as isize - if v < 0.0 { 1 } else { 0 };
        glm::vec2(to_tile(coord.x), to_tile(coord.y))
    }

    pub fn tile_to_world(coord: TileCoord) -> glm::Vec2 {
        let tile_size = glmh::vec2_fill(TILE_WIDTH as f32);
        let coord: glm::Vec2 = glm::convert(coord);
        let coord = coord.component_mul(&tile_size);
        coord + tile_size / 2.0
    }

    pub fn iter_tile_coords_by_distance(
        target: glm::Vec2,
        max_distance: f32,
    ) -> impl Iterator<Item = DistantTileCoord> {
        let box_start = target - glmh::vec2_fill(max_distance);
        let box_end = target + glmh::vec2_fill(max_distance);

        let box_start = Self::world_to_tile(box_start);
        let box_end = Self::world_to_tile(box_end) + glm::vec2(1, 1);

        let mut within = Vec::new();

        for y in box_start.y..box_end.y {
            for x in box_start.x..box_end.x {
                let coord = glm::vec2(x, y);
                let world = Self::tile_to_world(coord);
                let distance = glm::distance(&target, &world);

                if distance < max_distance {
                    within.push(DistantTileCoord { distance, coord });
                }
            }
        }

        within.sort_by(|a, b| OrderedFloat::from(a.distance).cmp(&OrderedFloat::from(b.distance)));

        within.into_iter()
    }

    pub fn graze(&mut self, coord: TileCoord) -> bool {
        if let Some(before) = self.grid.get_mut(coord) {
            if let Some(grazed) = grazed(*before) {
                *before = grazed;
                return true;
            }
        }
        false
    }
}

pub type TileGraphics = Vec<glm::IVec2>;

pub struct TileAutomata {
    pub func: Box<dyn Fn(TileCoord, &mut SparseGrid2d<tile::Type>, &mut ThreadRng)>,
}
pub struct TileTypeData {
    pub graphics: TileGraphics,
    pub automata: Vec<Option<TileAutomata>>,
}

impl TileTypeData {
    pub fn new() -> Self {
        ////graphics
        let mut graphics: [Option<glm::IVec2>; tile::TYPE_COUNT] = [None; tile::TYPE_COUNT];

        graphics[tile::SOIL] = Some(glm::vec2(0, 0));
        graphics[tile::GRASS_GROWN] = Some(glm::vec2(1, 1));
        graphics[tile::GRASS_GRAZED] = Some(glm::vec2(0, 1));
        graphics[tile::SLIME_GROWN] = Some(glm::vec2(1, 2));
        graphics[tile::SLIME_GRAZED] = Some(glm::vec2(0, 2));

        ////automata
        let mut automata = Vec::with_capacity(tile::TYPE_COUNT);
        automata.resize_with(tile::TYPE_COUNT, Default::default);

        automata[tile::GRASS_GROWN] = Some(TileAutomata {
            func: Box::new(|my_coord, tiles: &mut SparseGrid2d<tile::Type>, rng| {
                const SPREAD_CHANCE: f64 = 0.064;
                const TAKE_OVER_SLIME_CHANCE: f64 = 0.1;

                if rng.gen_bool(SPREAD_CHANCE) {
                    let chosen = tiles
                        .non_empty_neighbors_nesw(my_coord)
                        .filter_map(|n| {
                            if n.tile == &tile::SOIL
                                || (n.tile == &tile::SLIME_GRAZED
                                    && rng.gen_bool(TAKE_OVER_SLIME_CHANCE))
                            {
                                Some(n.coord)
                            } else {
                                None
                            }
                        })
                        .choose(&mut rand::thread_rng());

                    if let Some(chosen) = chosen {
                        tiles.set(chosen, tile::GRASS_GROWN);
                    }
                }
            }),
        });
        automata[tile::GRASS_GRAZED] = Some(TileAutomata {
            func: Box::new(|my_coord, tiles, rng| {
                const CHANGE_CHANCE: f64 = 0.03;
                const GROW_BACK_VS_DIE_ODDS: u32 = 2;
                const OUT_OF_ODDS: u32 = 4;

                if rng.gen_bool(CHANGE_CHANCE) {
                    if rng.gen_ratio(GROW_BACK_VS_DIE_ODDS, OUT_OF_ODDS) {
                        tiles.set(my_coord, tile::GRASS_GROWN);
                    } else {
                        tiles.set(my_coord, tile::SOIL);
                    }
                }
            }),
        });
        automata[tile::SLIME_GROWN] = Some(TileAutomata {
            func: Box::new(|my_coord, tiles: &mut SparseGrid2d<tile::Type>, rng| {
                const SPREAD_CHANCE: f64 = 0.064;
                const TAKE_OVER_GRASS_CHANCE: f64 = 0.1;

                if rng.gen_bool(SPREAD_CHANCE) {
                    let chosen = tiles
                        .non_empty_neighbors_nesw(my_coord)
                        .filter_map(|n| {
                            if n.tile == &tile::SOIL
                                || (n.tile == &tile::GRASS_GRAZED
                                    && rng.gen_bool(TAKE_OVER_GRASS_CHANCE))
                            {
                                Some(n.coord)
                            } else {
                                None
                            }
                        })
                        .choose(&mut rand::thread_rng());

                    if let Some(chosen) = chosen {
                        tiles.set(chosen, tile::SLIME_GROWN);
                    }
                }
            }),
        });
        automata[tile::SLIME_GRAZED] = Some(TileAutomata {
            func: Box::new(|my_coord, tiles, rng| {
                const CHANGE_CHANCE: f64 = 0.03;
                const GROW_BACK_VS_DIE_ODDS: u32 = 2;
                const OUT_OF_ODDS: u32 = 4;

                if rng.gen_bool(CHANGE_CHANCE) {
                    if rng.gen_ratio(GROW_BACK_VS_DIE_ODDS, OUT_OF_ODDS) {
                        tiles.set(my_coord, tile::SLIME_GROWN);
                    } else {
                        tiles.set(my_coord, tile::SOIL);
                    }
                }
            }),
        });

        Self {
            graphics: graphics.iter().map(|t| t.unwrap()).collect(),
            automata,
        }
    }
}

pub fn _grazeable(t: tile::Type) -> bool {
    grazed(t).is_some()
}

pub fn grazed(t: tile::Type) -> Option<tile::Type> {
    match t {
        tile::GRASS_GROWN => Some(tile::GRASS_GRAZED),
        tile::SLIME_GROWN => Some(tile::SLIME_GRAZED),
        _ => None,
    }
}
