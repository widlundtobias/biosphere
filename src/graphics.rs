use dpx_db::table::AutoId;
use dpx_db::table::Table;
use habanero::render::RendererId;

pub const DEFAULT_ZOOM: f32 = 1.0;

pub const QUAD_RENDERER: RendererId =
    RendererId::new(unsafe { std::num::NonZeroUsize::new_unchecked(1) });
pub const DEBUG_RENDERER: RendererId =
    RendererId::new(unsafe { std::num::NonZeroUsize::new_unchecked(2) });
pub struct Graphics {
    pub render_settings: habanero::render::RenderSettings,
    pub shader: habanero::render::ShaderId,
    pub quad_renderer: RendererId,
    pub debug_renderer: RendererId,

    pub disabled_debug: Option<habanero::render::RenderPass>,
}

impl Graphics {
    pub fn new(
        window_size: glm::UVec2,
        texture_t: &mut habanero::render::TTexture,
        shader_t: &mut habanero::render::TShader,
        renderer_t: &mut habanero::render::TRenderer,
        viewport_t: &mut habanero::render::TViewport,
        camera_t: &mut habanero::render::TCamera,
        render_pass_t: &mut habanero::render::TRenderPass,
        gl: std::rc::Rc<habanero::gl::Gl>,
    ) -> Self {
        //rendering settings
        let render_settings = habanero::render::RenderSettings::new(texture_t, gl.clone());

        //we only need one shader and this is the one
        let shader = shader_t
            .insert(habanero::render_2d::sprite_shader::new_sprite_shader(
                gl.clone(),
            ))
            .id;

        //set up our renderers
        let quad_renderer = QUAD_RENDERER;
        habanero::render::add_renderer(
            quad_renderer,
            renderer_t,
            habanero::render::QuadRenderer::new_with_builders(
                vec![
                    Box::new(habanero::render_2d::SpriteQuadBuilder::new()),
                    Box::new(habanero::render_2d::TilemapQuadBuilder::new()),
                ],
                gl.clone(),
            ),
        );
        let debug_renderer = DEBUG_RENDERER;
        habanero::render::add_renderer(
            debug_renderer,
            renderer_t,
            habanero::render::DebugRenderer::new(gl),
        );

        //viewports and camera
        let viewport = viewport_t
            .insert(habanero::render::Viewport {
                start: glm::vec2(0, 0),
                size: window_size,
                clear_color: habanero::color::Color::opaque(100, 100, 100),
            })
            .id;

        let camera = camera_t
            .insert(habanero::render::Camera::new(
                habanero::render::Projection::new_orthographic(
                    glm::convert(window_size),
                    DEFAULT_ZOOM,
                    -100.0,
                    100.0,
                ),
            ))
            .id;

        //configure render passes
        render_pass_t.insert(habanero::render::RenderPass {
            order: 1,
            viewport,
            renderer: quad_renderer,
            camera,
        });
        render_pass_t.insert(habanero::render::RenderPass {
            order: 2,
            viewport,
            renderer: debug_renderer,
            camera,
        });

        Self {
            render_settings,
            shader,
            quad_renderer,
            debug_renderer,
            disabled_debug: None,
        }
    }

    pub fn _set_world_camera_position(pos: glm::Vec2, cam_t: &mut habanero::render::TCamera) {
        let tr = &mut cam_t.first_mut().unwrap().data.translation;
        tr.x = pos.x;
        tr.y = pos.y;
    }

    pub fn toggle_dren(&mut self, render_pass_t: &mut habanero::render::TRenderPass) {
        let active = render_pass_t
            .iter()
            .find(|e| e.data.renderer == self.debug_renderer)
            .map(|e| e.id);

        match active {
            Some(e) => {
                let pass = render_pass_t.remove(e).unwrap(); //should be unwrappable at this point
                self.disabled_debug = Some(pass);
            }
            None => {
                render_pass_t.insert(self.disabled_debug.take().unwrap()); //should be unwrappable at this point
            }
        }
    }
}
