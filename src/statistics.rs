use std::{fs::File, io::Write, path::Path};

use dpx_db::table::Table;
use serde::Serialize;
use world::tile;

use crate::{db::DbAll, world};

const DATUM_FREQUENCY: u32 = 60;

pub struct Datum {
    time: usize,
    grown_tiles: usize,
    grazed_tiles: usize,
    soil_tiles: usize,

    herbivores: usize,
    predators: usize,
}

pub struct Statistics {
    data: Vec<Datum>,
    cooldown: u32,
    time_counter: usize,
}

#[derive(Serialize)]
pub struct StatisticsFlat {
    data: StatisticsFlatData,
    title: String,
}
#[derive(Serialize)]
pub struct StatisticsFlatData {
    time: Vec<usize>,
    grown_tiles: Vec<usize>,
    grazed_tiles: Vec<usize>,
    soil_tiles: Vec<usize>,
    herbivores: Vec<usize>,
    predators: Vec<usize>,
}

impl StatisticsFlat {
    pub fn new(to_reserve_for: usize) -> Self {
        Self {
            data: StatisticsFlatData {
                time: Vec::with_capacity(to_reserve_for),
                grown_tiles: Vec::with_capacity(to_reserve_for),
                grazed_tiles: Vec::with_capacity(to_reserve_for),
                soil_tiles: Vec::with_capacity(to_reserve_for),
                herbivores: Vec::with_capacity(to_reserve_for),
                predators: Vec::with_capacity(to_reserve_for),
            },
            title: "TMP_TITLE".to_string(),
        }
    }
}

impl From<Statistics> for StatisticsFlat {
    fn from(s: Statistics) -> Self {
        let mut res = Self::new(s.data.len());

        for d in s.data {
            res.data.time.push(d.time);
            res.data.grown_tiles.push(d.grown_tiles);
            res.data.grazed_tiles.push(d.grazed_tiles);
            res.data.soil_tiles.push(d.soil_tiles);
            res.data.herbivores.push(d.herbivores);
            res.data.predators.push(d.predators);
        }

        res
    }
}

impl Statistics {
    pub fn new() -> Self {
        Self {
            data: Vec::new(),
            cooldown: DATUM_FREQUENCY,
            time_counter: 0,
        }
    }

    pub fn record_datum(&mut self, db: &DbAll, world: &world::World) {
        let total_creatures = db.creature.physical_stats_t.len();
        let herbivores = db.creature.prey_t.len();
        let predators = total_creatures - herbivores;

        if self.cooldown == 0 {
            self.cooldown = DATUM_FREQUENCY - 1;
            self.data.push(Datum {
                time: self.time_counter,
                soil_tiles: world
                    .tiles
                    .grid
                    .iter_tiles()
                    .filter(|t| t.tile == &tile::SOIL)
                    .count(),
                grazed_tiles: world
                    .tiles
                    .grid
                    .iter_tiles()
                    .filter(|t| t.tile == &tile::GRASS_GRAZED)
                    .count(),
                grown_tiles: world
                    .tiles
                    .grid
                    .iter_tiles()
                    .filter(|t| t.tile == &tile::GRASS_GROWN)
                    .count(),
                herbivores,
                predators,
            });
        } else {
            self.cooldown -= 1;
        }
        self.time_counter += 1;
    }

    pub fn write_to_disk(&mut self) {
        let mut new_statistics = Statistics::new();
        std::mem::swap(self, &mut new_statistics);

        let flat = StatisticsFlat::from(new_statistics);

        let string = serde_json::to_string(&flat).unwrap();

        let path = Path::new("statistics.json");
        let display = path.display();

        let mut file = match File::create(&path) {
            Err(why) => panic!("couldn't create {}: {}", display, why),
            Ok(file) => file,
        };

        match file.write_all(string.as_bytes()) {
            Err(why) => panic!("couldn't write to {}: {}", display, why),
            Ok(_) => println!("successfully wrote to {}", display),
        }
    }
}
