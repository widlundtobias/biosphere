use habanero::{color::Color, entity::EntityProperties};
use habanero::{entity::add_property, render::add_color_properties};
use habanero::{render_2d::add_animated_sprite_properties, spatial_2d::add_physics_properties};

use crate::{entities::creature, world::tile};

pub fn herbivore(
    position: glm::Vec2,
    color: Color,
    res: &crate::resources::ResourceIdiot,
) -> EntityProperties {
    let props = add_animated_sprite_properties(
        EntityProperties::new(),
        position,
        super::CREATURE_DEPTH,
        None,
        None,
        Some(glm::vec2(1.0, 1.0) * 2.0),
        Some(res.textures.herbivore),
        res.sprite_shader,
        res.animations.herbivore_walk,
    );

    let props = add_color_properties(props, color);
    let props = add_physics_properties(props);

    let props = add_property(
        props,
        creature::ai::BoxedAi::new(creature::ai::herbivore::HerbivoreAi::new_explore()),
    );

    let props = add_property(
        props,
        creature::PhysicalStats {
            max_speed: 0.4,
            max_acc: 0.02,
        },
    );

    let props = add_property(
        props,
        creature::Prey {
            prey_type: creature::PreyType::Earthen,
        },
    );
    let props = add_property(
        props,
        creature::Herbivore {
            diet: tile::GRASS_GROWN,
        },
    );
    let props = add_property(
        props,
        creature::Breeder {
            birth_type: creature::BirthType::Herbivore,
        },
    );

    add_property(props, creature::Energy::new_half(10000))
}

pub fn slimovore(
    position: glm::Vec2,
    color: Color,
    res: &crate::resources::ResourceIdiot,
) -> EntityProperties {
    let props = add_animated_sprite_properties(
        EntityProperties::new(),
        position,
        super::CREATURE_DEPTH,
        None,
        None,
        Some(glm::vec2(1.0, 1.0) * 2.0),
        Some(res.textures.slimovore),
        res.sprite_shader,
        res.animations.slimovore_walk,
    );

    let props = add_color_properties(props, color);
    let props = add_physics_properties(props);

    let props = add_property(
        props,
        creature::ai::BoxedAi::new(creature::ai::herbivore::HerbivoreAi::new_explore()),
    );

    let props = add_property(
        props,
        creature::PhysicalStats {
            max_speed: 0.7,
            max_acc: 0.02,
        },
    );

    let props = add_property(
        props,
        creature::Prey {
            prey_type: creature::PreyType::Slimic,
        },
    );
    let props = add_property(
        props,
        creature::Herbivore {
            diet: tile::SLIME_GROWN,
        },
    );
    let props = add_property(
        props,
        creature::Breeder {
            birth_type: creature::BirthType::Slimovore,
        },
    );

    add_property(props, creature::Energy::new_half(10000))
}

pub fn predator(
    position: glm::Vec2,
    color: Color,
    res: &crate::resources::ResourceIdiot,
) -> EntityProperties {
    let props = add_animated_sprite_properties(
        EntityProperties::new(),
        position,
        super::CREATURE_DEPTH,
        None,
        None,
        Some(glm::vec2(1.0, 1.0) * 2.0),
        Some(res.textures.predator),
        res.sprite_shader,
        res.animations.predator_walk,
    );

    let props = add_color_properties(props, color);
    let props = add_physics_properties(props);

    let props = add_property(
        props,
        creature::ai::BoxedAi::new(creature::ai::predator::PredatorAi::new_explore()),
    );

    let props = add_property(
        props,
        creature::PhysicalStats {
            max_speed: 0.6,
            max_acc: 0.021,
        },
    );
    let props = add_property(
        props,
        creature::Breeder {
            birth_type: creature::BirthType::Predator,
        },
    );

    let props = add_property(
        props,
        creature::Predator {
            diet: creature::PreyType::Earthen,
            prey_detection_range: 60.0,
        },
    );

    add_property(props, creature::Energy::new_half(10000))
}

pub fn alien(
    position: glm::Vec2,
    color: Color,
    res: &crate::resources::ResourceIdiot,
) -> EntityProperties {
    let props = add_animated_sprite_properties(
        EntityProperties::new(),
        position,
        super::CREATURE_DEPTH,
        None,
        None,
        Some(glm::vec2(1.0, 1.0) * 2.0),
        Some(res.textures.alien),
        res.sprite_shader,
        res.animations.alien_walk,
    );

    let props = add_color_properties(props, color);
    let props = add_physics_properties(props);

    let props = add_property(
        props,
        creature::ai::BoxedAi::new(creature::ai::predator::PredatorAi::new_explore()),
    );

    let props = add_property(
        props,
        creature::PhysicalStats {
            max_speed: 0.3,
            max_acc: 0.021,
        },
    );
    let props = add_property(
        props,
        creature::Breeder {
            birth_type: creature::BirthType::Alien,
        },
    );

    let props = add_property(
        props,
        creature::Predator {
            diet: creature::PreyType::Slimic,
            prey_detection_range: 5000.0,
        },
    );

    add_property(props, creature::Energy::new_half(100000))
}
