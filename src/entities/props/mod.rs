mod creature;

pub use creature::*;

pub const CREATURE_DEPTH: f32 = 0.5;
pub const TILEMAP_DEPTH: f32 = 0.1;
