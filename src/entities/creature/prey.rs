use habanero::hash;
use habanero::hash::{Hash, TypeHash};

#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum PreyType {
    Earthen,
    Slimic,
}
#[derive(Debug, Clone, Copy)]
pub struct Prey {
    pub prey_type: PreyType,
}
impl TypeHash for Prey {
    fn type_hash() -> Hash {
        hash!("Prey")
    }
}
