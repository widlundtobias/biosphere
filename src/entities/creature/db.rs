use super::*;
use ai::BoxedAi;
use dpx_db::mapvectable::MapVecTable;
use dpx_db::table;
use habanero::{entity::EntityId, table::TableVisitablePerId, table::TableVisitorPerId};

pub type TAi = MapVecTable<EntityId, BoxedAi, table::ManualIdTag>;
pub type TMoveIntention = MapVecTable<EntityId, MoveIntention, table::ManualIdTag>;
pub type TPhysicalStats = MapVecTable<EntityId, PhysicalStats, table::ManualIdTag>;
pub type TEnergy = MapVecTable<EntityId, Energy, table::ManualIdTag>;
pub type TDeath = MapVecTable<EntityId, Death, table::ManualIdTag>;
pub type TBirth = MapVecTable<BirthId, Birth, table::AutoIdTag>;
pub type TPrey = MapVecTable<EntityId, Prey, table::ManualIdTag>;
pub type TPredator = MapVecTable<EntityId, Predator, table::ManualIdTag>;
pub type THerbivore = MapVecTable<EntityId, Herbivore, table::ManualIdTag>;
pub type TBreeder = MapVecTable<EntityId, Breeder, table::ManualIdTag>;

pub struct Module {
    pub ai_t: TAi,
    pub move_intention_t: TMoveIntention,
    pub physical_stats_t: TPhysicalStats,
    pub energy_t: TEnergy,
    pub death_t: TDeath,
    pub birth_t: TBirth,
    pub prey_t: TPrey,
    pub predator_t: TPredator,
    pub herbivore_t: THerbivore,
    pub breeder_t: TBreeder,
}

impl Module {
    pub fn new() -> Self {
        Self {
            ai_t: TAi::new(),
            move_intention_t: TMoveIntention::new(),
            physical_stats_t: TPhysicalStats::new(),
            energy_t: TEnergy::new(),
            death_t: TDeath::new(),
            birth_t: TBirth::new(),
            prey_t: TPrey::new(),
            predator_t: TPredator::new(),
            herbivore_t: THerbivore::new(),
            breeder_t: TBreeder::new(),
        }
    }
}

impl TableVisitablePerId<EntityId> for Module {
    fn visit_manual_id_mut<Visitor: TableVisitorPerId<EntityId>>(&mut self, visitor: &Visitor) {
        visitor.visit_manual_id_mut(&mut self.ai_t);
        visitor.visit_manual_id_mut(&mut self.move_intention_t);
        visitor.visit_manual_id_mut(&mut self.physical_stats_t);
        visitor.visit_manual_id_mut(&mut self.energy_t);
        visitor.visit_manual_id_mut(&mut self.death_t);
        visitor.visit_manual_id_mut(&mut self.prey_t);
        visitor.visit_manual_id_mut(&mut self.predator_t);
        visitor.visit_manual_id_mut(&mut self.herbivore_t);
        visitor.visit_manual_id_mut(&mut self.breeder_t);
    }
    fn visit_manual_id<Visitor: TableVisitorPerId<EntityId>>(&self, visitor: &Visitor) {
        visitor.visit_manual_id(&self.ai_t);
        visitor.visit_manual_id(&self.move_intention_t);
        visitor.visit_manual_id(&self.physical_stats_t);
        visitor.visit_manual_id(&self.energy_t);
        visitor.visit_manual_id(&self.death_t);
        visitor.visit_manual_id(&self.prey_t);
        visitor.visit_manual_id(&self.predator_t);
        visitor.visit_manual_id(&self.herbivore_t);
        visitor.visit_manual_id(&self.breeder_t);
    }
}
