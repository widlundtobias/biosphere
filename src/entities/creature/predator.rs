use super::*;

use habanero::hash;
use habanero::hash::{Hash, TypeHash};

#[derive(Debug, Clone, Copy)]
pub struct Predator {
    pub diet: PreyType,
    pub prey_detection_range: f32,
}
impl TypeHash for Predator {
    fn type_hash() -> Hash {
        hash!("Predator")
    }
}
