use habanero::hash;
use habanero::hash::{Hash, TypeHash};

pub const WELL_FED_TOP_PERC: f32 = 0.25;
pub const STARVING_BOTTOM_PERC: f32 = 0.1;

pub const DEFAULT_ENERGY_COST: u32 = 2;
pub const BABY_ENERGY_COST: u32 = 5000;

pub const STARVING_DEATH_CHANCE: f64 = 0.002;

#[derive(Debug, Clone, Copy, Eq, PartialEq, Ord, PartialOrd)]
pub enum EnergyStatus {
    WellFed,
    Normal,
    Starving,
}
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub struct Energy {
    pub current: u32,
    pub max: u32,
}
impl TypeHash for Energy {
    fn type_hash() -> Hash {
        hash!("Energy")
    }
}

impl Energy {
    pub fn new_half(max: u32) -> Self {
        Self {
            current: max / 2,
            max,
        }
    }

    pub fn percent_full(&self) -> f32 {
        self.current as f32 / self.max as f32
    }

    pub fn status(&self) -> EnergyStatus {
        let perc = self.percent_full();

        if perc < STARVING_BOTTOM_PERC {
            EnergyStatus::Starving
        } else if perc > 1.0 - WELL_FED_TOP_PERC {
            EnergyStatus::WellFed
        } else {
            EnergyStatus::Normal
        }
    }

    pub fn consume(&mut self, amount: u32) -> u32 {
        let consumed = amount.min(self.current);
        self.current -= consumed;
        consumed
    }

    pub fn refill(&mut self, amount: u32) -> u32 {
        let to_max = self.max - self.current;
        let filled = amount.min(to_max);

        self.current += filled;

        filled
    }
}
