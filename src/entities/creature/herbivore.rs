use habanero::hash;
use habanero::hash::{Hash, TypeHash};

use crate::world::tile;

#[derive(Debug, Clone, Copy)]
pub struct Herbivore {
    pub diet: tile::Type,
}
impl TypeHash for Herbivore {
    fn type_hash() -> Hash {
        hash!("Herbivore")
    }
}
