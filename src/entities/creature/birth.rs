use dpx_db::{mapvectable::ManualId, table::Table};
use habanero::{color::Color, color::NColor, entity::EntityId, hash};
use habanero::{
    glmh,
    hash::{Hash, TypeHash},
};
extern crate palette;
use palette::{Hsv, Hue};
use rand::Rng;

use crate::{
    db::DbAll,
    entities::{self},
    resources::ResourceIdiot,
};

use super::{Energy, MoveIntention};

#[derive(Debug, Clone, Copy)]
pub enum BirthType {
    Herbivore,
    Slimovore,
    Predator,
    Alien,
}

dpx_db::declare_table_id!(BirthId);
#[derive(Debug, Clone, Copy)]
pub struct Birth {
    pub parent: EntityId,
    pub energy_for_child: u32,
    pub birth_type: BirthType,
}
impl TypeHash for Birth {
    fn type_hash() -> Hash {
        hash!("Birth")
    }
}

impl Birth {
    pub fn new(parent: EntityId, energy_for_child: u32, birth_type: BirthType) -> Self {
        Self {
            parent,
            energy_for_child,
            birth_type,
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub struct Breeder {
    pub birth_type: BirthType,
}
impl TypeHash for Breeder {
    fn type_hash() -> Hash {
        hash!("Breeder")
    }
}

pub fn update_birth(db: &mut DbAll, res: &ResourceIdiot) {
    let births = db.creature.birth_t.data.clone();
    db.creature.birth_t.clear();

    for birth in births {
        if let (Some(parent_pos), Some(parent_color), Some(parent_energy)) = (
            db.hab_spatial_2d
                .world_position_2d_t
                .get(birth.parent)
                .cloned(),
            db.hab_render.drawable_color_t.get(birth.parent).cloned(),
            db.creature.energy_t.get(birth.parent).cloned(),
        ) {
            let offset = glmh::rand::direction_2d(&mut rand::thread_rng()) * 4.0;
            let spawn_pos = parent_pos.coord + offset;

            ////choose hereditary color
            let norm_col = NColor::from(parent_color.color);
            let col: Hsv = palette::rgb::Srgb::new(norm_col.r, norm_col.g, norm_col.b).into();
            let col = col.shift_hue(rand::thread_rng().gen_range(-5.0, 5.0));
            let col = palette::rgb::Srgb::from(col);
            let col = NColor::opaque(col.red, col.green, col.blue);
            let col = Color::from(col);

            let props = match birth.birth_type {
                BirthType::Herbivore => entities::props::herbivore(spawn_pos, col, res),
                BirthType::Slimovore => entities::props::slimovore(spawn_pos, col, res),
                BirthType::Predator => entities::props::predator(spawn_pos, col, res),
                BirthType::Alien => entities::props::alien(spawn_pos, col, res),
            };

            let child = habanero::entity::insert_from_props(db.next_id(), db, &props);

            let away_force = 0.3;
            let away_dir = spawn_pos - parent_pos.coord;
            if let Some(phys) = db.hab_spatial_2d.physics_2d_t.get_mut(child) {
                phys.velocity = away_dir * away_force;
            }
            if let Some(phys) = db.hab_spatial_2d.physics_2d_t.get_mut(birth.parent) {
                phys.velocity = -away_dir * away_force;
            }
            db.creature
                .move_intention_t
                .set(birth.parent, MoveIntention::stop());
            db.creature
                .move_intention_t
                .set(child, MoveIntention::stop());

            db.creature.energy_t.set(
                child,
                Energy {
                    current: birth.energy_for_child,
                    max: parent_energy.max,
                },
            );
        }
    }
}
