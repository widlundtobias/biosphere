use std::fmt::Debug;

use crate::{entities::creature, resources::ResourceIdiot, world};

use creature::{Energy, TPredator, TPrey};

use habanero::{entity::EntityId, hash, render_2d::AnimatedSprite, spatial_2d::TWorldPosition2d};
use habanero::{hash::TypeHash, render_2d::TAnimatedSprite};
use rand::prelude::ThreadRng;

#[derive(Debug, Clone)]
pub struct BoxedAi {
    pub ai: Box<dyn Ai>,
}

impl TypeHash for BoxedAi {
    fn type_hash() -> habanero::hash::Hash {
        hash!("BoxedAi")
    }
}

impl BoxedAi {
    pub fn new<T: Ai>(t: T) -> Self {
        Self { ai: Box::new(t) }
    }
}

pub trait AiClone {
    fn clone_box(&self) -> Box<dyn Ai>;
}

impl<T: 'static + Ai + Clone> AiClone for T {
    fn clone_box(&self) -> Box<dyn Ai> {
        Box::new(self.clone())
    }
}

impl Clone for Box<dyn Ai> {
    fn clone(&self) -> Self {
        self.clone_box()
    }
}

pub trait Ai: 'static + AiClone + Debug {
    fn update(
        &mut self,
        id: EntityId,
        animated_sprite: &mut AnimatedSprite,
        energy: &mut Energy,
        pos: glm::Vec2,
        rng: &mut ThreadRng,
        tiles: &mut world::Tiles,
        res: &ResourceIdiot,
        position_t: &TWorldPosition2d,
        move_intention_t: &mut creature::TMoveIntention,
        birth_t: &mut creature::TBirth,
        death_t: &mut creature::TDeath,
        prey_t: &TPrey,
        herbivore_t: &creature::THerbivore,
        predator_t: &creature::TPredator,
        breeder_t: &creature::TBreeder,
    );
}

pub fn update_ai(
    tiles: &mut world::Tiles,
    res: &ResourceIdiot,
    ai_t: &mut creature::TAi,
    energy_t: &mut creature::TEnergy,
    position_t: &TWorldPosition2d,
    move_intention_t: &mut creature::TMoveIntention,
    animated_sprite_t: &mut TAnimatedSprite,
    birth_t: &mut creature::TBirth,
    death_t: &mut creature::TDeath,
    prey_t: &TPrey,
    herbivore_t: &creature::THerbivore,
    predator_t: &TPredator,
    breeder_t: &creature::TBreeder,
) {
    let rng = &mut rand::thread_rng();

    dpx_macros::join!(
        join_iter = JoinEntry,
        ai_t: ai,
        energy_t: energy,
        position_t: pos,
        animated_sprite_t: animated_sprite
    );
    for JoinEntry {
        id,
        ai,
        energy,
        pos,
        animated_sprite,
    } in join_iter
    {
        ai.ai.update(
            id,
            animated_sprite,
            energy,
            pos.coord,
            rng,
            tiles,
            res,
            position_t,
            move_intention_t,
            birth_t,
            death_t,
            prey_t,
            herbivore_t,
            predator_t,
            breeder_t,
        );
    }
}
