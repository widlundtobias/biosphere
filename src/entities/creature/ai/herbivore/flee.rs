use habanero::{glmh, spatial_2d::TWorldPosition2d};

use crate::entities::creature;

#[derive(Debug, Copy, Clone)]
pub struct FleeMood {
    pub remaining: u32,
}

impl FleeMood {
    pub fn new() -> Self {
        Self {
            remaining: FLEE_EXTRA_TIME,
        }
    }
}

pub const PREDATOR_DETECT_DISTANCE: f32 = 55.0;
pub const FLEE_EXTRA_TIME: u32 = 120;

pub fn predator_close(
    pos: glm::Vec2,
    prey: &creature::Prey,
    position_t: &TWorldPosition2d,
    predator_t: &creature::TPredator,
) -> bool {
    dpx_macros::join!(join_iter = JoinEntry, position_t: pos, predator_t: predator);

    join_iter.any(|e| {
        e.predator.diet == prey.prey_type
            && glm::distance(&e.pos.coord, &pos) <= PREDATOR_DETECT_DISTANCE
    })
}

pub fn flee_from_predator_vec(
    pos: glm::Vec2,
    prey: &creature::Prey,
    position_t: &TWorldPosition2d,
    predator_t: &creature::TPredator,
) -> glm::Vec2 {
    let mut compounded = glmh::CompoundedVec2::new();

    dpx_macros::join!(join_iter = JoinEntry, position_t: pos, predator_t: predator);

    for predator in join_iter.filter(|e| {
        e.predator.diet == prey.prey_type
            && glm::distance(&e.pos.coord, &pos) <= PREDATOR_DETECT_DISTANCE
    }) {
        let to_predator = predator.pos.coord - pos;
        compounded.push(-to_predator, glm::distance(&predator.pos.coord, &pos));
    }

    compounded.compounded().unwrap_or_default()
}
