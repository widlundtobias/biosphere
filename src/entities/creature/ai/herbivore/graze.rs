use crate::world::TileCoord;

#[derive(Debug, Copy, Clone)]
pub struct GrazeMood {
    pub fsm: GrazeFsm,
    pub target_tile: TileCoord,
}

impl GrazeMood {
    pub fn new(target_tile: TileCoord) -> Self {
        Self {
            fsm: GrazeFsm::Approach,
            target_tile,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum GrazeFsm {
    Approach,   //next: Graze
    Graze(u32), //data: time to graze, next: switch to ExploreMood
}

pub const GRAZE_TIME: u32 = 40;
pub const GRAZE_DISTANCE: f32 = 4.0;
pub const GRASS_ENERGY_CONTENT: u32 = 1204;

impl GrazeFsm {
    pub fn graze() -> Self {
        GrazeFsm::Graze(GRAZE_TIME)
    }
}
