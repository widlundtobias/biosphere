use std::fmt::Debug;

use crate::{entities::creature, resources::ResourceIdiot, world};

use super::*;

use dpx_db::mapvectable::{AutoId, ManualId};
use habanero::{entity::EntityId, render_2d::AnimatedSprite, spatial_2d::TWorldPosition2d};

use dpx_db::table::Table;
use rand::{prelude::ThreadRng, Rng};
use rand_distr::{Distribution, Normal};
use world::TileCoord;

#[derive(Debug, Copy, Clone)]
pub enum HerbivoreAi {
    Explore(ExploreMood),
    Graze(GrazeMood),
    Flee(FleeMood),
}

const DEBUG: bool = false;

const AFRAID: bool = true;

impl creature::ai::Ai for HerbivoreAi {
    fn update(
        &mut self,
        id: EntityId,
        animated_sprite: &mut AnimatedSprite,
        energy: &mut creature::Energy,
        pos: glm::Vec2,
        rng: &mut ThreadRng,
        tiles: &mut world::Tiles,
        res: &ResourceIdiot,
        position_t: &TWorldPosition2d,
        move_intention_t: &mut creature::TMoveIntention,
        birth_t: &mut creature::TBirth,
        death_t: &mut creature::TDeath,
        prey_t: &creature::TPrey,
        herbivore_t: &creature::THerbivore,
        predator_t: &creature::TPredator,
        breeder_t: &creature::TBreeder,
    ) {
        let dist = Normal::new(1.0, 0.2).unwrap();

        let mut to_consume = creature::DEFAULT_ENERGY_COST;
        let mut next = None;
        let mut switch_to = |state, do_override| {
            if next.is_none() || do_override {
                next = Some(state)
            }
        };

        let diet = match herbivore_t.get(id) {
            Some(herbivore) => herbivore.diet,
            None => return,
        };

        let prey = prey_t.get(id);

        let should_flee = || {
            AFRAID && prey.is_some() && predator_close(pos, prey.unwrap(), position_t, predator_t)
        }; //unwrap safe, guarded by if

        match self {
            HerbivoreAi::Explore(explore) => match &mut explore.fsm {
                ExploreFsm::ChooseExplorationDir => {
                    //this state chooses a general direction in which to
                    //explore. this is done instantly, then the state is
                    //changed to wait
                    animated_sprite.change_to(res.animations.herbivore_walk);

                    let exploration_dir = Some(habanero::glmh::rand::direction_2d(rng));

                    if DEBUG {
                        println!(
                            "[{}]: chose dir [{},{}] for exploration, will wait",
                            id,
                            exploration_dir.unwrap().x,
                            exploration_dir.unwrap().y
                        );
                    }

                    switch_to(
                        HerbivoreAi::Explore(ExploreMood {
                            exploration_dir,
                            fsm: ExploreFsm::wait(rng),
                            ..*explore
                        }),
                        false,
                    );
                }
                ExploreFsm::Wait(remaining) => {
                    //simple wait state that waits the remaining time, then moves on
                    //to the next state: choosing walk direction
                    //
                    //if we are well fed, there's a chance to spawn a babby

                    if *remaining > 0 {
                        *remaining -= 1;

                        //mebbe babby
                        if let (creature::EnergyStatus::WellFed, Some(breeder)) =
                            (energy.status(), breeder_t.get(id))
                        {
                            to_consume = creature::BABY_ENERGY_COST;

                            birth_t.insert(creature::Birth::new(
                                id,
                                to_consume,
                                breeder.birth_type,
                            ));
                        }
                    } else {
                        if DEBUG {
                            println!("[{}]: done waiting, will choose walk dir", id);
                        }
                        switch_to(
                            HerbivoreAi::Explore(ExploreMood {
                                fsm: ExploreFsm::ChooseWalkDir,
                                ..*explore
                            }),
                            false,
                        );
                    }

                    //flee if applicable
                    if should_flee() {
                        switch_to(HerbivoreAi::new_flee(), true);
                    }
                }
                ExploreFsm::ChooseWalkDir => {
                    //this state chooses a specific direction in which to
                    //walk, based on a random deviation from the exploration direction. this is done instantly,
                    //then the state is changed to walk

                    let exploration_dir = explore.exploration_dir.unwrap(); //should be set in this state, or bug

                    let exploration_angle = habanero::glmh::dir_2d_to_angle(exploration_dir);

                    let deviation_factor: f32 = dist.sample(rng);
                    let deviation_factor = deviation_factor.max(0.5).min(1.5);

                    let walk_dir = Some(habanero::glmh::angle_to_dir_2d(
                        exploration_angle * deviation_factor,
                    ));

                    if DEBUG {
                        println!(
                            "[{}]: chose dir [{},{}] (dev: {}) for walking, will walk",
                            id,
                            walk_dir.unwrap().x,
                            walk_dir.unwrap().y,
                            deviation_factor
                        );
                    }
                    switch_to(
                        HerbivoreAi::Explore(ExploreMood {
                            walk_dir,
                            fsm: ExploreFsm::walk(rng),
                            ..*explore
                        }),
                        false,
                    );
                }
                ExploreFsm::Walk(remaining) => {
                    //walks in the walk direction for some time, then randomly
                    //moves on to either picking new exploration, or waiting
                    //
                    //if grazz is found within the detection distance, then switch to graze it

                    to_consume = creature::DEFAULT_ENERGY_COST + 1;

                    if *remaining > 0 {
                        *remaining -= 1;

                        if let Some(to_graze) = world::Tiles::iter_tile_coords_by_distance(
                            pos,
                            explore::GRASS_DETECT_DISTANCE,
                        )
                        .filter_map(|t| tiles.grid.entry_ref(t.coord))
                        .find(|e| *e.tile == diet)
                        {
                            //found sweet grass, 420 graze it
                            switch_to(HerbivoreAi::new_graze(to_graze.coord), false);
                        } else {
                            //just move

                            //check if off the map, in which case, go to centre
                            let my_tile = world::Tiles::world_to_tile(pos);
                            if tiles.grid.get(my_tile).is_none() {
                                //move back to centre
                                explore.walk_dir = Some(-pos.normalize());
                            }

                            //move along walk dir
                            move_intention_t.set(
                                id,
                                creature::MoveIntention::direction(
                                    explore.walk_dir.unwrap(),
                                    100.0,
                                )
                                .unwrap(), //unwrap safe since it should be set here, or bug.  100 to make it magnitudey
                            );
                        }
                    } else {
                        //stop moving
                        move_intention_t.set(id, creature::MoveIntention::stop());

                        //decide to pick exploration, or wait
                        if DEBUG {
                            println!(
                                "[{}]: done walking, might choose new exploration dir, or wait",
                                id
                            );
                        }
                        if rng.gen_ratio(1, 4) {
                            //pick exploration

                            switch_to(
                                HerbivoreAi::Explore(ExploreMood {
                                    fsm: ExploreFsm::ChooseExplorationDir,
                                    ..*explore
                                }),
                                false,
                            );
                        } else {
                            //wait

                            switch_to(
                                HerbivoreAi::Explore(ExploreMood {
                                    fsm: ExploreFsm::wait(rng),
                                    ..*explore
                                }),
                                false,
                            );
                        }
                    }

                    //flee if applicable
                    if should_flee() {
                        switch_to(HerbivoreAi::new_flee(), true);
                    }
                }
            },
            HerbivoreAi::Graze(graze) => match &mut graze.fsm {
                GrazeFsm::Approach => {
                    //walks in the direction of the target grass tile to graze
                    //when close enough, start grazing it.
                    //
                    //also check if it is still grassy and not grazed by others, in which case explore

                    if tiles.grid.get(graze.target_tile) == Some(&diet) {
                        //the grass is still there, keep going for it

                        let target = world::Tiles::tile_to_world(graze.target_tile);
                        let distance = glm::distance(&pos, &target);

                        if distance <= GRAZE_DISTANCE {
                            //stop moving to graze
                            move_intention_t.set(id, creature::MoveIntention::stop());

                            //instantly set tile to grazed
                            if tiles.graze(graze.target_tile) {
                                //give energy
                                energy.refill(GRASS_ENERGY_CONTENT);
                            }

                            //switch to grazing
                            switch_to(
                                HerbivoreAi::Graze(GrazeMood {
                                    fsm: GrazeFsm::graze(),
                                    ..*graze
                                }),
                                false,
                            );
                        } else {
                            //move to grazing target
                            move_intention_t.set(
                                id,
                                creature::MoveIntention::direction(target - pos, 100.0)
                                    .unwrap_or_else(creature::MoveIntention::stop),
                            );
                        }
                    } else {
                        //someone beat us to it, grass is gone. just explore again
                        switch_to(HerbivoreAi::new_explore(), false);
                    }

                    //flee if applicable
                    if should_flee() {
                        switch_to(HerbivoreAi::new_flee(), true);
                    }
                }
                GrazeFsm::Graze(remaining) => {
                    //stand there and chew for a while... then explore more
                    animated_sprite.change_to(res.animations.herbivore_eat);

                    if *remaining == 0 {
                        //done grazing, time to find more snacks
                        switch_to(HerbivoreAi::new_explore(), false)
                    } else {
                        //keep chewing that yummy grass
                        *remaining -= 1;
                    }

                    //flee if applicable
                    if should_flee() {
                        switch_to(HerbivoreAi::new_flee(), true);
                    }
                }
            },
            HerbivoreAi::Flee(FleeMood { remaining }) => {
                if *remaining > 0 {
                    if predator_close(pos, prey.unwrap(), position_t, predator_t) {
                        //unwrwap safe - why would we flee if we aren't prey?
                        *remaining = FLEE_EXTRA_TIME;
                    } else {
                        *remaining -= 1;
                    }

                    //check if off the map
                    let my_tile = world::Tiles::world_to_tile(pos);
                    let flee_dir = if tiles.grid.get(my_tile).is_none() {
                        //off the map, become paralyzed by fear!
                        glm::zero()
                    } else {
                        //run run!
                        flee_from_predator_vec(pos, prey.unwrap(), position_t, predator_t)
                    };

                    move_intention_t.set(
                        id,
                        creature::MoveIntention::direction(flee_dir, 100.0)
                            .unwrap_or_else(creature::MoveIntention::stop),
                    );
                } else {
                    switch_to(HerbivoreAi::new_explore(), false);
                }
            }
        };

        if let Some(next) = next {
            *self = next;
        }

        let old_status = energy.status();
        energy.consume(to_consume);
        let new_status = energy.status();
        if new_status != old_status && DEBUG {
            println!("[{}]: new state: {:?}", id, new_status);
        }

        let mut starved = false;
        if energy.current == 0 {
            starved = true;
            if DEBUG {
                println!("[{}]: starved at zero", id);
            }
        }
        if matches!(energy.status(), creature::EnergyStatus::Starving)
            && rng.gen_bool(creature::STARVING_DEATH_CHANCE)
        {
            starved = true;
            if DEBUG {
                println!(
                    "[{}]: starved by chance with remaining: {}",
                    id, energy.current
                );
            }
        }

        if starved {
            death_t.insert(id, creature::Death::new(res.animations.herbivore_dead));
        }
    }
}

impl HerbivoreAi {
    pub fn new_explore() -> Self {
        HerbivoreAi::Explore(ExploreMood::new())
    }
    pub fn new_graze(target_tile: TileCoord) -> Self {
        HerbivoreAi::Graze(GrazeMood::new(target_tile))
    }
    pub fn new_flee() -> Self {
        HerbivoreAi::Flee(FleeMood::new())
    }
}
