pub use self::boxed_ai::*;

mod boxed_ai;

pub mod herbivore;
pub mod predator;
