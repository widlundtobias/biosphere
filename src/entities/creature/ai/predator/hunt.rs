use habanero::entity::EntityId;

#[derive(Debug, Copy, Clone)]

pub struct HuntMood {
    pub fsm: HuntFsm,
    pub target_creature: EntityId,
}

impl HuntMood {
    pub fn new(target_creature: EntityId) -> Self {
        Self {
            fsm: HuntFsm::Chase,
            target_creature,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum HuntFsm {
    Chase,    //next: Hunt
    Eat(u32), //data: time to hunt, next: switch to ExploreMood
}

pub const EAT_TIME: u32 = 540;
pub const EAT_DISTANCE: f32 = 3.0;
pub const PREY_ENERGY_CONTENT: u32 = 1504;

impl HuntFsm {
    pub fn eat() -> Self {
        HuntFsm::Eat(EAT_TIME)
    }
}
