#[derive(Debug, Copy, Clone)]

pub struct ExploreMood {
    pub fsm: ExploreFsm,
    pub exploration_dir: Option<glm::Vec2>,
    pub walk_dir: Option<glm::Vec2>,
}

impl ExploreMood {
    pub fn new() -> Self {
        Self {
            fsm: ExploreFsm::ChooseExplorationDir,
            exploration_dir: None,
            walk_dir: None,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub enum ExploreFsm {
    ChooseExplorationDir, //next: Wait
    Wait(u32),            //data: time to wait, next: ChooseWalkDir
    ChooseWalkDir,        //next: Walk
    Walk(u32),            //data: time to walk, next: random(ChooseExplorationDir, Wait)
}

pub const WAIT_MIN: u32 = 30;
pub const WAIT_MAX: u32 = 70;

pub const WALK_MIN: u32 = 30;
pub const WALK_MAX: u32 = 200;

impl ExploreFsm {
    pub fn wait(rng: &mut impl rand::Rng) -> Self {
        ExploreFsm::Wait(rng.gen_range(WAIT_MIN, WAIT_MAX))
    }
    pub fn walk(rng: &mut impl rand::Rng) -> Self {
        ExploreFsm::Walk(rng.gen_range(WALK_MIN, WALK_MAX))
    }
}
