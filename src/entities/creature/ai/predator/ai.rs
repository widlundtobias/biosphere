use std::fmt::Debug;

use crate::{entities::creature, resources::ResourceIdiot, world};

use super::*;

use dpx_db::{
    mapvectable::{AutoId, ManualId},
    table::Table,
};
use habanero::{entity::EntityId, render_2d::AnimatedSprite, spatial_2d::TWorldPosition2d};

use rand::{prelude::ThreadRng, Rng};
use rand_distr::{Distribution, Normal};

#[derive(Debug, Copy, Clone)]
pub enum PredatorAi {
    Explore(ExploreMood),
    Hunt(HuntMood),
}

const DEBUG: bool = false;

impl creature::ai::Ai for PredatorAi {
    fn update(
        &mut self,
        id: EntityId,
        animated_sprite: &mut AnimatedSprite,
        energy: &mut creature::Energy,
        pos: glm::Vec2,
        rng: &mut ThreadRng,
        tiles: &mut world::Tiles,
        res: &ResourceIdiot,
        position_t: &TWorldPosition2d,
        move_intention_t: &mut creature::TMoveIntention,
        birth_t: &mut creature::TBirth,
        death_t: &mut creature::TDeath,
        prey_t: &creature::TPrey,
        _herbivore_t: &creature::THerbivore,
        predator_t: &creature::TPredator,
        breeder_t: &creature::TBreeder,
    ) {
        let dist = Normal::new(1.0, 0.2).unwrap();

        let diet = match predator_t.get(id) {
            Some(predator) => predator.diet,
            None => return,
        };

        let creature_detect_distance = match predator_t.get(id) {
            Some(predator) => predator.prey_detection_range,
            None => return,
        };

        let mut to_consume = creature::DEFAULT_ENERGY_COST + 1;
        *self = match self {
            PredatorAi::Explore(explore) => match &mut explore.fsm {
                ExploreFsm::ChooseExplorationDir => {
                    //this state chooses a general direction in which to
                    //explore. this is done instantly, then the state is
                    //changed to wait
                    animated_sprite.change_to(res.animations.predator_walk);

                    let exploration_dir = Some(habanero::glmh::rand::direction_2d(rng));

                    if DEBUG {
                        println!(
                            "[{}]: chose dir [{},{}] for exploration, will wait",
                            id,
                            exploration_dir.unwrap().x,
                            exploration_dir.unwrap().y
                        );
                    }
                    PredatorAi::Explore(ExploreMood {
                        exploration_dir,
                        fsm: ExploreFsm::wait(rng),
                        ..*explore
                    })
                }
                ExploreFsm::Wait(remaining) => {
                    //simple wait state that waits the remaining time, then moves on
                    //to the next state: choosing walk direction
                    //
                    //if we are well fed, there's a chance to spawn a babby

                    if *remaining > 0 {
                        *remaining -= 1;

                        //mebbe babby
                        if let (creature::EnergyStatus::WellFed, Some(breeder)) =
                            (energy.status(), breeder_t.get(id))
                        {
                            to_consume = creature::BABY_ENERGY_COST;

                            birth_t.insert(creature::Birth::new(
                                id,
                                to_consume,
                                breeder.birth_type,
                            ));
                        }

                        PredatorAi::Explore(ExploreMood { ..*explore })
                    } else {
                        if DEBUG {
                            println!("[{}]: done waiting, will choose walk dir", id);
                        }
                        PredatorAi::Explore(ExploreMood {
                            fsm: ExploreFsm::ChooseWalkDir,
                            ..*explore
                        })
                    }
                }
                ExploreFsm::ChooseWalkDir => {
                    //this state chooses a specific direction in which to
                    //walk, based on a random deviation from the exploration direction. this is done instantly,
                    //then the state is changed to walk

                    let exploration_dir = explore.exploration_dir.unwrap(); //should be set in this state, or bug

                    let exploration_angle = habanero::glmh::dir_2d_to_angle(exploration_dir);

                    let deviation_factor: f32 = dist.sample(rng);
                    let deviation_factor = deviation_factor.max(0.5).min(1.5);

                    let walk_dir = Some(habanero::glmh::angle_to_dir_2d(
                        exploration_angle * deviation_factor,
                    ));

                    if DEBUG {
                        println!(
                            "[{}]: chose dir [{},{}] (dev: {}) for walking, will walk",
                            id,
                            walk_dir.unwrap().x,
                            walk_dir.unwrap().y,
                            deviation_factor
                        );
                    }
                    PredatorAi::Explore(ExploreMood {
                        walk_dir,
                        fsm: ExploreFsm::walk(rng),
                        ..*explore
                    })
                }
                ExploreFsm::Walk(remaining) => {
                    //walks in the walk direction for some time, then randomly
                    //moves on to either picking new exploration, or waiting
                    //
                    //if grazz is found within the detection distance, then switch to hunt it

                    to_consume = creature::DEFAULT_ENERGY_COST + 2;

                    if *remaining > 0 {
                        *remaining -= 1;

                        let mut candidates: Vec<_> = position_t
                            .iter()
                            .filter(|e| e.id != id)
                            .filter(|e| {
                                glm::distance(&e.data.coord, &pos) < creature_detect_distance
                            })
                            .filter(|e| prey_t.get(e.id).map(|p| p.prey_type) == Some(diet))
                            .collect();
                        candidates.sort_by(|a, b| {
                            let a_dist = glm::distance(&a.data.coord, &pos);
                            let b_dist = glm::distance(&b.data.coord, &pos);

                            ordered_float::OrderedFloat::from(a_dist)
                                .cmp(&ordered_float::OrderedFloat::from(b_dist))
                        });
                        let target = candidates.first().map(|c| c.id);

                        if let Some(target) = target {
                            //found sweet grass, 420 hunt it
                            PredatorAi::new_hunt(target)
                        } else {
                            //just move

                            //check if off the map, in which case, go to centre
                            let my_tile = world::Tiles::world_to_tile(pos);
                            if tiles.grid.get(my_tile).is_none() {
                                //move back to centre
                                explore.walk_dir = Some(-pos.normalize());
                            }

                            //move along walk dir
                            move_intention_t.set(
                                id,
                                creature::MoveIntention::direction(
                                    explore.walk_dir.unwrap(),
                                    100.0,
                                )
                                .unwrap(), //unwrap safe since it should be set here, or bug.  100 to make it magnitudey
                            );
                            PredatorAi::Explore(*explore)
                        }
                    } else {
                        //stop moving
                        move_intention_t.set(id, creature::MoveIntention::stop());

                        //decide to pick exploration, or wait
                        if DEBUG {
                            println!(
                                "[{}]: done walking, might choose new exploration dir, or wait",
                                id
                            );
                        }
                        if rng.gen_ratio(1, 4) {
                            //pick exploration

                            PredatorAi::Explore(ExploreMood {
                                fsm: ExploreFsm::ChooseExplorationDir,
                                ..*explore
                            })
                        } else {
                            //wait

                            PredatorAi::Explore(ExploreMood {
                                fsm: ExploreFsm::wait(rng),
                                ..*explore
                            })
                        }
                    }
                }
            },
            PredatorAi::Hunt(hunt) => match &mut hunt.fsm {
                HuntFsm::Chase => {
                    //walks in the direction of the target grass tile to hunt
                    //when close enough, start grazing it.
                    //
                    //also check if it is still grassy and not huntd by others, in which case explore

                    let target = position_t.get(hunt.target_creature).map(|e| e.coord);

                    if let Some(target) = target {
                        //the grass is still there, keep going for it

                        let distance = glm::distance(&pos, &target);

                        if distance <= EAT_DISTANCE {
                            //stop moving to hunt
                            move_intention_t.set(id, creature::MoveIntention::stop());

                            //instantly set tile to huntd
                            death_t.set(hunt.target_creature, creature::Death::instant());
                            //give energy
                            energy.refill(PREY_ENERGY_CONTENT);

                            //switch to grazing
                            PredatorAi::Hunt(HuntMood {
                                fsm: HuntFsm::eat(),
                                ..*hunt
                            })
                        } else {
                            //move to grazing target
                            move_intention_t.set(
                                id,
                                creature::MoveIntention::direction(target - pos, 100.0)
                                    .unwrap_or_else(creature::MoveIntention::stop),
                            );

                            PredatorAi::Hunt(*hunt)
                        }
                    } else {
                        //someone beat us to it, grass is gone. just explore again
                        PredatorAi::new_explore()
                    }
                }
                HuntFsm::Eat(remaining) => {
                    //stand there and chew for a while... then explore more
                    animated_sprite.change_to(res.animations.predator_eat);

                    to_consume = 0;

                    if *remaining == 0 {
                        //done grazing, time to find more snacks
                        PredatorAi::new_explore()
                    } else {
                        //keep chewing that yummy grass
                        *remaining -= 1;
                        PredatorAi::Hunt(*hunt)
                    }
                }
            },
        };

        let old_status = energy.status();
        energy.consume(to_consume);
        let new_status = energy.status();
        if new_status != old_status && DEBUG {
            println!("[{}]: new state: {:?}", id, new_status);
        }

        let mut starved = false;
        if energy.current == 0 {
            starved = true;
            if DEBUG {
                println!("[{}]: starved at zero", id);
            }
        }
        if matches!(energy.status(), creature::EnergyStatus::Starving)
            && rng.gen_bool(creature::STARVING_DEATH_CHANCE)
        {
            starved = true;
            if DEBUG {
                println!(
                    "[{}]: starved by chance with remaining: {}",
                    id, energy.current
                );
            }
        }

        if starved {
            death_t.insert(id, creature::Death::new(res.animations.predator_dead));
        }
    }
}

impl PredatorAi {
    pub fn new_explore() -> Self {
        PredatorAi::Explore(ExploreMood::new())
    }
    pub fn new_hunt(target_creature: EntityId) -> Self {
        PredatorAi::Hunt(HuntMood::new(target_creature))
    }
}
