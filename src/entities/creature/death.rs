use dpx_db::{mapvectable::ManualId, table::Table};
use habanero::hash::{Hash, TypeHash};
use habanero::{hash, render_2d::SpriteAnimationId};

use crate::db::DbAll;

use super::MoveIntention;

const CORPSE_REMOVE_TIME: u32 = 1200;

#[derive(Debug, Clone, Copy)]
pub struct Death {
    pub ttl: u32,
    animation: Option<SpriteAnimationId>,
}

impl Death {
    pub fn new(animation: SpriteAnimationId) -> Self {
        Self {
            ttl: CORPSE_REMOVE_TIME,
            animation: Some(animation),
        }
    }
    pub fn instant() -> Self {
        Self {
            ttl: 1,
            animation: None,
        }
    }
}

impl TypeHash for Death {
    fn type_hash() -> Hash {
        hash!("Death")
    }
}

pub fn update_dead(db: &mut DbAll) {
    let mut to_remove = Vec::new();

    for dead in &mut db.creature.death_t {
        dead.data.ttl -= 1;

        if dead.data.ttl == 0 {
            to_remove.push(dead.id)
        } else {
            db.creature.ai_t.remove(dead.id);
            db.creature
                .move_intention_t
                .set(dead.id, MoveIntention::stop());
            if let Some(sprite) = db.hab_render_2d.animated_sprite_t.get_mut(dead.id) {
                if let Some(anim) = dead.data.animation {
                    sprite.change_to(anim);
                }
            }
        }
    }

    for gone in to_remove {
        habanero::entity::erase_entity_data(gone, db);
    }
}
