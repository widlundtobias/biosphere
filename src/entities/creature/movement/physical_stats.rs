use habanero::hash;
use habanero::hash::{Hash, TypeHash};
#[derive(Debug, Clone, Copy)]
pub struct PhysicalStats {
    pub max_speed: f32,
    pub max_acc: f32,
}
impl TypeHash for PhysicalStats {
    fn type_hash() -> Hash {
        hash!("PhysicalStats")
    }
}
