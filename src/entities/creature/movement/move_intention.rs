use crate::entities::creature::{TMoveIntention, TPhysicalStats};

use habanero::hash;
use habanero::{
    glmh,
    hash::{Hash, TypeHash},
};

///This vector is not normalized. The magnitude represents how fast the desired movement is
#[derive(Debug, Clone, Copy)]
pub struct MoveIntention {
    pub vector: glm::Vec2,
}
impl TypeHash for MoveIntention {
    fn type_hash() -> Hash {
        hash!("MoveIntention")
    }
}

impl MoveIntention {
    pub fn stop() -> Self {
        Self {
            vector: glm::zero(),
        }
    }
    pub fn direction(direction: glm::Vec2, magnitude: f32) -> Option<Self> {
        glmh::safe_normalize(&direction).map(|v| Self {
            vector: v * magnitude,
        })
    }
}

pub fn apply_move_intention(
    physics_2d_t: &mut habanero::spatial_2d::TPhysics2d,
    move_intention_t: &TMoveIntention,
    physical_stats_t: &TPhysicalStats,
) {
    dpx_macros::join!(
        join_iter = JoinEntry,
        physics_2d_t: phys,
        move_intention_t: move_intention,
        physical_stats_t: physical_stats
    );

    for JoinEntry {
        phys,
        move_intention,
        physical_stats,
        id: _,
    } in join_iter
    {
        let direction =
            habanero::glmh::safe_normalize(&move_intention.vector).unwrap_or_else(glm::zero);
        let desired_speed = glm::length(&move_intention.vector);

        phys.acceleration = habanero::spatial_2d::accelerate(
            direction,
            phys.velocity,
            physical_stats.max_speed.min(desired_speed),
            physical_stats.max_acc,
        );
    }
}
