#![allow(clippy::new_without_default)]

extern crate nalgebra_glm as glm;
extern crate sdl2;

extern crate rust_hawktracer;
use rust_hawktracer::*;

mod db;
mod entities;
mod game;
mod graphics;
mod resources;
mod scenario;
mod statistics;
mod ui;
mod world;

fn main() {
    let instance = HawktracerInstance::new();
    let _listener = instance.create_listener(HawktracerListenerType::ToFile {
        file_path: "trace.bin".into(),
        buffer_size: 4096,
    });

    let game = game::Game::new().unwrap();

    emscripten_main_loop::run(game);
}
