use habanero::color;

use crate::{
    db::DbAll,
    entities::props::{alien, herbivore, predator, slimovore},
    resources::ResourceIdiot,
    world::tile,
    world::World,
};

pub fn test_scenario(db: &mut DbAll, world: &mut World, res: &ResourceIdiot) {
    let soil = glm::vec2(0, 0);
    let sand = glm::vec2(1, 0);
    let grazed = glm::vec2(0, 1);
    let tall = glm::vec2(1, 1);

    let _tiles = [&soil, &sand, &grazed, &tall];

    for y in -20..20 {
        for x in -20..20 {
            world.tiles.grid.set(glm::vec2(x, y), tile::SOIL);
        }
    }

    for y in -20..-17 {
        for x in -5..6 {
            world.tiles.grid.set(glm::vec2(x, y), tile::GRASS_GROWN);
        }
    }
    for y in 17..20 {
        for x in -5..6 {
            world.tiles.grid.set(glm::vec2(x, y), tile::GRASS_GROWN);
        }
    }
    for y in -5..6 {
        for x in -20..-17 {
            world.tiles.grid.set(glm::vec2(x, y), tile::GRASS_GROWN);
        }
    }
    for y in -5..6 {
        for x in 17..20 {
            world.tiles.grid.set(glm::vec2(x, y), tile::GRASS_GROWN);
        }
    }

    for y in -5..6 {
        for x in -5..6 {
            world.tiles.grid.set(glm::vec2(x, y), tile::SLIME_GRAZED);
        }
    }

    //creatures
    let creature_props = herbivore(glm::vec2(-288.0, -20.0), color::yellow(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(-288.0, 0.0), color::yellow(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(-288.0, 20.0), color::yellow(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);

    let creature_props = herbivore(glm::vec2(288.0, -20.0), color::blue(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(288.0, 0.0), color::blue(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(288.0, 20.0), color::blue(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);

    let creature_props = herbivore(glm::vec2(-20.0, -288.0), color::teal(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(0.0, -288.0), color::teal(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(20.0, -288.0), color::teal(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);

    let creature_props = herbivore(glm::vec2(-20.0, 288.0), color::gray(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(0.0, 288.0), color::gray(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = herbivore(glm::vec2(20.0, 288.0), color::gray(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);

    let creature_props = predator(glm::vec2(-228.0, 0.0), color::red(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = predator(glm::vec2(228.0, 0.0), color::pink(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);

    let creature_props = slimovore(glm::vec2(-20.0, 28.0), color::green(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = slimovore(glm::vec2(0.0, 41.0), color::green(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
    let creature_props = slimovore(glm::vec2(10.0, 35.0), color::green(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);

    let creature_props = alien(glm::vec2(0.0, 0.0), color::white(), res);
    habanero::entity::insert_from_props(db.next_id(), db, &creature_props);
}
