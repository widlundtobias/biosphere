use dpx_db::table::AutoId;
use habanero::gl::{FilterMode, Gl, Texture, TextureConfiguration, WrapMode};
use habanero::render::{ShaderId, TShader, TTexture, TextureId};
use habanero::render_2d::{AnimationMode, SpriteAnimation, SpriteAnimationId, TSpriteAnimation};

pub struct Textures {
    pub tilemap: TextureId,
    pub herbivore: TextureId,
    pub predator: TextureId,
    pub slimovore: TextureId,
    pub alien: TextureId,
}

pub struct Animations {
    pub herbivore_walk: SpriteAnimationId,
    pub herbivore_eat: SpriteAnimationId,
    pub herbivore_dead: SpriteAnimationId,
    pub slimovore_walk: SpriteAnimationId,
    pub slimovore_eat: SpriteAnimationId,
    pub slimovore_dead: SpriteAnimationId,
    pub predator_walk: SpriteAnimationId,
    pub predator_eat: SpriteAnimationId,
    pub predator_dead: SpriteAnimationId,
    pub alien_walk: SpriteAnimationId,
    pub alien_eat: SpriteAnimationId,
    pub alien_dead: SpriteAnimationId,
}

impl Animations {
    pub fn new(animation_t: &mut TSpriteAnimation) -> Self {
        Self {
            herbivore_walk: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 0), glm::vec2(8, 8), 2, 15)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            herbivore_eat: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 8), glm::vec2(8, 8), 2, 7)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            herbivore_dead: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 16), glm::vec2(8, 8), 1, 7)
                        .with_mode(AnimationMode::Stop),
                )
                .id,
            slimovore_walk: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 0), glm::vec2(8, 8), 2, 15)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            slimovore_eat: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 8), glm::vec2(8, 8), 2, 7)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            slimovore_dead: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 16), glm::vec2(8, 8), 1, 7)
                        .with_mode(AnimationMode::Stop),
                )
                .id,
            predator_walk: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 0), glm::vec2(10, 10), 2, 11)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            predator_eat: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 10), glm::vec2(10, 10), 2, 4)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            predator_dead: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 20), glm::vec2(10, 10), 1, 4)
                        .with_mode(AnimationMode::Stop),
                )
                .id,
            alien_walk: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 0), glm::vec2(10, 10), 2, 11)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            alien_eat: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 10), glm::vec2(10, 10), 2, 4)
                        .with_mode(AnimationMode::Loop),
                )
                .id,
            alien_dead: animation_t
                .insert(
                    SpriteAnimation::new(glm::vec2(0, 20), glm::vec2(10, 10), 1, 4)
                        .with_mode(AnimationMode::Stop),
                )
                .id,
        }
    }
}

impl Textures {
    pub fn new(texture_t: &mut TTexture, gl: std::rc::Rc<Gl>) -> Self {
        let config = TextureConfiguration {
            generate_mipmaps: false,
            filter_mode: FilterMode::Nearest,
            wrap_mode: WrapMode::Clamp,
        };

        let mut load = |path| {
            let t = Texture::new_rgba_from_image(
                image::open(path).unwrap().as_rgba8().unwrap(),
                &config,
                gl.clone(),
            );

            texture_t.insert(t).id
        };

        Self {
            tilemap: load("assets/tiles/tilemap.png"),
            herbivore: load("assets/sprites/herbivore.png"),
            predator: load("assets/sprites/predator.png"),
            slimovore: load("assets/sprites/slimovore.png"),
            alien: load("assets/sprites/alien.png"),
        }
    }
}

pub struct ResourceIdiot {
    pub textures: Textures,
    pub animations: Animations,
    pub sprite_shader: ShaderId,
}

impl ResourceIdiot {
    pub fn new(
        shader_t: &mut TShader,
        texture_t: &mut TTexture,
        animation_t: &mut TSpriteAnimation,
        gl: std::rc::Rc<Gl>,
    ) -> Self {
        Self {
            textures: Textures::new(texture_t, gl.clone()),
            animations: Animations::new(animation_t),
            sprite_shader: shader_t
                .insert(habanero::render_2d::sprite_shader::new_sprite_shader(gl))
                .id,
        }
    }
}
