use dpx_db::table::Table;
use entities::creature;
use habanero::entity::EntityId;
use maplit::hashmap;

use crate::ui;
use crate::world;
use crate::{db, scenario::test_scenario};
use crate::{entities, graphics};
use crate::{resources, statistics};
use rust_hawktracer::*;

pub struct Game {
    input: habanero::input::InputSystem,
    graphics: graphics::Graphics,
    ui: ui::Ui,
    resources: resources::ResourceIdiot,
    world: world::World,

    tilemap: EntityId,

    game_speed: u32,
    statistics: statistics::Statistics,

    db: db::DbAll,
    window: habanero::window::GlWindow,
}

impl Game {
    pub fn new() -> anyhow::Result<Self> {
        let window_size = glm::vec2(1366, 768);

        let window = habanero::window::GlWindow::new(
            "Biosphere",
            window_size,
            habanero::window::GlProfile::ES3,
        )?;

        let mut db = db::DbAll::new();

        let input = habanero::input::InputSystem::new(&window.sdl);

        let graphics = graphics::Graphics::new(
            window_size,
            &mut db.hab_render.texture_t,
            &mut db.hab_render.shader_t,
            &mut db.hab_render.renderer_t,
            &mut db.hab_render.viewport_t,
            &mut db.hab_render.camera_t,
            &mut db.hab_render.render_pass_t,
            window.gl.clone(),
        );

        let resources = resources::ResourceIdiot::new(
            &mut db.hab_render.shader_t,
            &mut db.hab_render.texture_t,
            &mut db.hab_render_2d.sprite_animation_t,
            window.gl.clone(),
        );

        let tilemap = habanero::entity::insert_from_props(
            db.next_id(),
            &mut db,
            &habanero::render_2d::add_tilemap_properties(
                habanero::entity::EntityProperties::new(),
                glm::zero(),
                entities::props::TILEMAP_DEPTH,
                None,
                None,
                glm::vec2(world::TILE_WIDTH as f32, world::TILE_WIDTH as f32),
                glm::vec2(8, 8),
                resources.textures.tilemap,
                resources.sprite_shader,
            ),
        );

        let mut res = Self {
            window,
            db,
            input,
            graphics,
            ui: ui::Ui::new(),
            resources,
            world: world::World::new(),
            tilemap,
            game_speed: 1,
            statistics: statistics::Statistics::new(),
        };

        test_scenario(&mut res.db, &mut res.world, &res.resources);

        Ok(res)
    }
}

impl emscripten_main_loop::MainLoop for Game {
    fn main_loop(&mut self) -> emscripten_main_loop::MainLoopEvent {
        scoped_tracepoint!(_main_loop);
        //////MARK NEW FRAME
        ////Rendering

        habanero::render::frame_start(&self.db.hab_render);

        //////INPUT
        let input = self.input.update_input_state(&self.window.sdl);
        let returned = self.ui.process_input(
            &input,
            &mut self.graphics,
            &mut self.db,
            &mut self.world,
            &self.resources,
            &mut self.game_speed,
        );

        //////LOGIC

        for _ in 0..self.game_speed {
            creature::ai::update_ai(
                &mut self.world.tiles,
                &self.resources,
                &mut self.db.creature.ai_t,
                &mut self.db.creature.energy_t,
                &self.db.hab_spatial_2d.world_position_2d_t,
                &mut self.db.creature.move_intention_t,
                &mut self.db.hab_render_2d.animated_sprite_t,
                &mut self.db.creature.birth_t,
                &mut self.db.creature.death_t,
                &self.db.creature.prey_t,
                &self.db.creature.herbivore_t,
                &self.db.creature.predator_t,
                &self.db.creature.breeder_t,
            );

            creature::update_birth(&mut self.db, &self.resources);
            creature::update_dead(&mut self.db);

            entities::creature::apply_move_intention(
                &mut self.db.hab_spatial_2d.physics_2d_t,
                &self.db.creature.move_intention_t,
                &self.db.creature.physical_stats_t,
            );

            habanero::spatial_2d::update_physics(
                &mut self.db.hab_spatial_2d.object_position_2d_t,
                &mut self.db.hab_spatial_2d.physics_2d_t,
            );

            habanero::spatial_2d::update_world_spatial(&mut self.db.hab_spatial_2d);

            self.world.tiles.update_automata();

            self.statistics.record_datum(&self.db, &self.world);
        }

        //////RENDERING
        if let Some(tilemap) = self.db.hab_render_2d.tilemap_t.get_mut(self.tilemap) {
            self.world.tiles.write_to_tilemap(tilemap);
        }
        ////dispatchers
        let sprite_quad_dispatcher = habanero::render_2d::SpriteQuadBuilderDispatcher {
            tables: (
                &self.db.hab_spatial_2d.world_position_2d_t,
                &self.db.hab_spatial_2d.world_rotation_2d_t,
                &self.db.hab_spatial_2d.world_depth_t,
                &self.db.hab_render_2d.sprite_t,
                &self.db.hab_render.drawable_shader_t,
                &self.db.hab_render.drawable_color_t,
                &self.db.hab_render_2d.sub_rect_sprite_t,
                &self.db.hab_render_2d.animated_sprite_t,
                &self.db.hab_render.shader_t,
                &self.db.hab_render.texture_t,
                &self.db.hab_render_2d.sprite_animation_t,
            ),
        };
        let tilemap_quad_dispatcher = habanero::render_2d::TilemapQuadBuilderDispatcher {
            tables: (
                &self.db.hab_spatial_2d.world_position_2d_t,
                &self.db.hab_spatial_2d.world_rotation_2d_t,
                &self.db.hab_spatial_2d.world_depth_t,
                &self.db.hab_render_2d.tilemap_t,
                &self.db.hab_render.drawable_shader_t,
                &self.db.hab_render.drawable_color_t,
                &self.db.hab_render.shader_t,
                &self.db.hab_render.texture_t,
            ),
        };

        let quad_dispatcher = habanero::render::QuadRendererDispatcher {
            builder_dispatchers: &hashmap! {
            habanero::render_2d::SPRITE_QUAD_BUILDER_ID => & sprite_quad_dispatcher as &dyn habanero::render::QuadBuilderDispatcher,
            habanero::render_2d::TILEMAP_QUAD_BUILDER_ID => & tilemap_quad_dispatcher as &dyn habanero::render::QuadBuilderDispatcher,
            },
        };
        let debug_dispatcher = habanero::render::DebugRendererDispatcher {
            shader: self.graphics.shader,
        };

        ////use dispatchers to render the frame
        {
            scoped_tracepoint!(_render_frame);
            habanero::render::render_frame(
            hashmap! {
                self.graphics.quad_renderer => &quad_dispatcher as &dyn habanero::render::RendererDispatcher,
                self.graphics.debug_renderer => &debug_dispatcher as &dyn habanero::render::RendererDispatcher,
            },
            &self.graphics.render_settings,
            &self.db.hab_render.render_pass_t,
            &mut self.db.hab_render.renderer_t,
            &self.db.hab_render.viewport_t,
            &self.db.hab_render.camera_t,
            &self.db.hab_render.texture_t,
            &self.db.hab_render.shader_t,
            &mut self.db.hab_render.uniform_location_t,
            &self.window.gl,
        )
        .unwrap();
        }

        ////swap window buffers
        {
            scoped_tracepoint!(_gl_swap_window);
            self.window.window.gl_swap_window();
        }

        ////tick animations
        habanero::render_2d::tick_animations(&mut self.db.hab_render_2d.animated_sprite_t);

        returned
    }
}

impl Drop for Game {
    fn drop(&mut self) {
        self.statistics.write_to_disk()
    }
}
