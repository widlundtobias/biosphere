use dpx_db::table::TableId;
use habanero::entity::EntityId;

use crate::entities::creature;

pub struct DbAll {
    pub hab_spatial_2d: habanero::spatial_2d::CoreModule,
    pub hab_render: habanero::render::CoreModule,
    pub hab_render_2d: habanero::render_2d::CoreModule,

    pub creature: creature::Module,

    next_id: habanero::entity::EntityId,
}

impl DbAll {
    pub fn new() -> Self {
        Self {
            hab_spatial_2d: habanero::spatial_2d::CoreModule::new(),
            hab_render: habanero::render::CoreModule::new(),
            hab_render_2d: habanero::render_2d::CoreModule::new(),
            creature: creature::Module::new(),
            next_id: habanero::entity::EntityId::start(),
        }
    }
    pub fn next_id(&mut self) -> EntityId {
        let id = self.next_id;
        self.next_id = self.next_id.next();
        id
    }
}

impl habanero::table::TableVisitablePerId<EntityId> for DbAll {
    fn visit_manual_id_mut<Visitor: habanero::table::TableVisitorPerId<EntityId>>(
        &mut self,
        visitor: &Visitor,
    ) {
        self.hab_spatial_2d.visit_manual_id_mut(visitor);
        self.hab_render.visit_manual_id_mut(visitor);
        self.hab_render_2d.visit_manual_id_mut(visitor);
        self.creature.visit_manual_id_mut(visitor);
    }
    fn visit_manual_id<Visitor: habanero::table::TableVisitorPerId<EntityId>>(
        &self,
        visitor: &Visitor,
    ) {
        self.hab_spatial_2d.visit_manual_id(visitor);
        self.hab_render.visit_manual_id(visitor);
        self.hab_render_2d.visit_manual_id(visitor);
        self.creature.visit_manual_id(visitor);
    }
}
