use crate::{db::DbAll, world};

use crate::{graphics, resources::ResourceIdiot};
use dpx_db::table::Table;
use habanero::collections::grid_2d::{east, north, south, west};

pub struct Ui {}

impl Ui {
    pub fn new() -> Self {
        Self {}
    }

    pub fn process_input(
        &mut self,
        input: &habanero::input::RawInput,
        graphics: &mut graphics::Graphics,
        db: &mut DbAll,
        world: &mut world::World,
        _res: &ResourceIdiot,
        game_speed: &mut u32,
    ) -> emscripten_main_loop::MainLoopEvent {
        let mut returned = emscripten_main_loop::MainLoopEvent::Continue;
        ////quit message
        if input.system_quit.is_some()
            || input
                .started_key_presses
                .contains(&sdl2::keyboard::Scancode::Q)
        {
            returned = emscripten_main_loop::MainLoopEvent::Terminate;
        }

        ////toggle dren
        if input
            .started_key_presses
            .contains(&sdl2::keyboard::Scancode::F8)
        {
            graphics.toggle_dren(&mut db.hab_render.render_pass_t);
        }

        ////handle resize message
        if let Some(resized) = input.window_resized {
            let x = resized.x;
            let y = resized.y;

            habanero::render::adjust_viewports_with(
                |v, index, count| {
                    let x_inc = (x as usize / count) as u32;
                    let index = index as u32;
                    let y = y as u32;

                    v.start = glm::vec2(index * x_inc, 0);
                    v.size = glm::vec2(x_inc, y);
                },
                &mut db.hab_render.viewport_t,
            );

            let cam = db.hab_render.camera_t.first_mut().unwrap().data;

            let zoom = cam
                .projection
                .as_orthographic_ref()
                .map(|o| o.zoom)
                .unwrap_or(graphics::DEFAULT_ZOOM);

            let new_size = glm::vec2(x as f32, y as f32);
            cam.projection =
                habanero::render::Projection::new_orthographic(new_size, zoom, -100.0, 100.0)
        }

        ////calculate mouse position in world just to have it
        let viewport = db.hab_render.viewport_t.first().unwrap().data;
        let camera = db.hab_render.camera_t.first().unwrap().data;

        let cursor_world =
            habanero::render::transform_window_to_world_2d(input.cursor_position, viewport, camera);
        let cursor_tile = world::Tiles::world_to_tile(cursor_world);

        ////clicking
        //draw tiles
        enum ToDraw {
            Graze,
            Grow,
        }
        let mut to_draw = None;

        if input
            .active_mouse_presses
            .contains(&sdl2::mouse::MouseButton::Left)
        {
            to_draw = Some(ToDraw::Graze);
        } else if input
            .active_mouse_presses
            .contains(&sdl2::mouse::MouseButton::Right)
        {
            to_draw = Some(ToDraw::Grow);
        }

        if let Some(to_draw) = to_draw {
            let brush = [
                cursor_tile,
                cursor_tile + north(),
                cursor_tile + east(),
                cursor_tile + south(),
                cursor_tile + west(),
            ];

            for &coord in brush.iter() {
                if world.tiles.grid.get(coord).is_some() {
                    match to_draw {
                        ToDraw::Graze => {
                            //graze grass by clicking it
                            world.tiles.graze(coord);
                        }
                        ToDraw::Grow => {
                            //just grow grass
                            world.tiles.grid.set(coord, world::tile::GRASS_GROWN);
                        }
                    }
                }
            }
        }

        ////rewind
        if input
            .started_key_presses
            .contains(&sdl2::keyboard::Scancode::Num1)
        {
            *game_speed = 1;
        }
        if input
            .started_key_presses
            .contains(&sdl2::keyboard::Scancode::Num2)
        {
            *game_speed = 10;
        }
        if input
            .started_key_presses
            .contains(&sdl2::keyboard::Scancode::Num3)
        {
            *game_speed = 100;
        }
        if input
            .started_key_presses
            .contains(&sdl2::keyboard::Scancode::Num4)
        {
            *game_speed = 1000;
        }

        ////display

        returned
    }
}
